const express = require('express');
const sqlite3 = require('sqlite3').verbose();

const app = express();
let db = new sqlite3.Database(':memory:');

db.serialize(function () {
    db.run('CREATE TABLE users (id BIGINT PRIMARY KEY ,name TEXT, password TEXT, email TEXT)');
    let putData = db.prepare('INSERT INTO users VALUES ( ?, ?, ?, ?)');
    const usersName = [
        {
            name: 'John Doe',
            password: 'qwerty',
            email: 'doe@example.com'
        },
        {
            name: 'John Koe',
            password: 'qwerty',
            email: 'koe@example.com'
        }
    ];

    for (let i = 0; i < usersName.length; i++) {
        putData.run(i + 1, usersName[i].name, usersName[i].password, usersName[i].email);
    }

    putData.finalize();
});

app.use(express.json(), (request, response, next) => {
    next()
});

async function query(queryValue) {
    return new Promise((resolve, reject) => {
        db.serialize(() => {
            db.all(`${queryValue}`, (err, value) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(value);
                }
            });
        });
    });
}

//GET all users
app.get('/users', async (request, response) => {
    const users = await query(`SELECT * FROM Users`)
    response.json({
        users: users
    });
});

//Get User by ID
app.get('/user', async (request, response) => {
    response.json({
        user: await query(`SELECT * FROM Users WHERE id = ${id = request.query.id}`)
    });
});

app.post('/user', async (request, response) => {
    if (!request.body)
        return response.sendStatus(400);
    else {
        const users = await query(`SELECT * FROM Users`);
        putData = db.prepare('INSERT INTO users VALUES ( ?, ?, ?, ?)');
        putData.run(users.length + 1, request.body.name, request.body.password, request.body.email);
        putData.finalize();
        return response.sendStatus(200);
    }
});

app.delete('/user', async (request, response) => {
    if (!request.body)
        return response.sendStatus(400);
    else{
        await query(`DELETE FROM users WHERE id = ${request.body.id}`)
        return response.sendStatus(200);
    }
});

app.listen(3000);
